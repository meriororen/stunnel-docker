FROM alpine
MAINTAINER Phillip Clark <phillip@flitbit.com>

COPY rootfs /

RUN set -ex &&\
    apk update && apk add --update stunnel &&\
    chmod +x /opt/run-stunnel.sh &&\
    rm -rf /tmp/* \
           /var/cache/apk/*

#echo "http://dl-3.alpinelinux.org/alpine/edge/testing/" >> /etc/apk/repositories &&\

EXPOSE 4442

ENTRYPOINT ["/opt/run-stunnel.sh"]
